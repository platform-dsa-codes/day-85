import java.util.Arrays;
import java.util.ArrayDeque;
import java.util.Deque;

class Solution {
    public int[] deckRevealedIncreasing(int[] deck) {
        // Sort the deck
        Arrays.sort(deck);
        
        // Initialize a deque to simulate the process
        Deque<Integer> deque = new ArrayDeque<>();
        
        // Add indices to the deque
        for (int i = 0; i < deck.length; i++) {
            deque.offer(i);
        }
        
        // Initialize the result array
        int[] result = new int[deck.length];
        
        // Reveal cards according to the process
        for (int card : deck) {
            result[deque.poll()] = card; // reveal the current card
            
            // Move the next top card to the bottom if there are remaining cards
            if (!deque.isEmpty()) {
                deque.offer(deque.poll());
            }
        }
        
        return result;
    }
}
