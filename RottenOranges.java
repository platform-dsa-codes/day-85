import java.util.LinkedList;
import java.util.Queue;

class Solution {
    public int orangesRotting(int[][] grid) {
        // Directions for 4-directional traversal (up, down, left, right)
        int[][] directions = {{-1, 0}, {1, 0}, {0, -1}, {0, 1}};
        
        int m = grid.length;
        int n = grid[0].length;
        
        Queue<int[]> queue = new LinkedList<>();
        
        // Count of fresh oranges
        int freshOranges = 0;
        
        // Initialize queue with rotten oranges and count fresh oranges
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (grid[i][j] == 2) {
                    queue.offer(new int[]{i, j, 0}); // Add position and time to queue
                } else if (grid[i][j] == 1) {
                    freshOranges++;
                }
            }
        }
        
        int minutes = 0;
        
        // Perform BFS traversal
        while (!queue.isEmpty()) {
            int[] curr = queue.poll();
            int x = curr[0];
            int y = curr[1];
            int time = curr[2];
            
            minutes = Math.max(minutes, time);
            
            // Traverse in 4 directions
            for (int[] dir : directions) {
                int newX = x + dir[0];
                int newY = y + dir[1];
                
                if (newX >= 0 && newX < m && newY >= 0 && newY < n && grid[newX][newY] == 1) {
                    grid[newX][newY] = 2; // Mark fresh orange as rotten
                    freshOranges--;
                    queue.offer(new int[]{newX, newY, time + 1}); // Add newly rotten orange to queue
                }
            }
        }
        
        return freshOranges == 0 ? minutes : -1;
    }
}
